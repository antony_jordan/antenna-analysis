.phony: clear execute html run-lab run-notebook

"NanoVNA Analysis.html":
	jupyter nbconvert --to html "NanoVNA Analysis.ipynb"

html: "NanoVNA Analysis.html"

clear:
	python -m nbconvert --ClearOutputPreprocessor.enabled=True --inplace "NanoVNA Analysis.ipynb"

execute:
	python -m nbconvert --execute --inplace "NanoVNA Analysis.ipynb"

venv:
	python -m venv venv
	venv/bin/pip install -r requirements.txt

run-lab:
	jupyter-lab

run-notebook:
	jupyter-notebook

